# Training

## Java

### Optionals

Esta clase nos permite el manejo de valores nulos previniendo la aparición de NullPointerException, introducida a partir de Java 8.

En este artículo oficial de Oracle se puede obtener más información sobre la importancia del uso de los optionals: https://www.oracle.com/technical-resources/articles/java/java8-optional.html.

Para crear objetos Optional podemos hacerlo de 3 formas diferentes:
* `Optional.empty()` crea un objeto optional sin valor.
* `Optional.of()` se usará este método cuando se está seguro que el objeto que se pasa al `of()` es no nulo, en caso contrario se obtendría un NullPointerException.
* `Optional.ofNullable()` con esta llamada podemos pasar un objeto como nulo, lo que equivaldrá a un `Optional.empty()`.

Se disponen de varios métodos para comprobar si un Optional está vacío o no:
* `Optional.isPresent()` devuelve si un optional no está vacío.
* `Optional.isEmpty()` lo contrario de `isPresent()`, disponible en Java 11.

Métodos para condiciones:
```java
final Optional<String> productName = Optional.ofNullable(product.getNeme()); 
productName.ifPresent(System.out::println);
```

Valores por defecto:

Hay dos métodos para realizar estas acciones: 
* `.orElse()` este recibe un valor.
* `orElseGet()` recibe un supplier.

Siempre y cuando podamos elegir es más eficiente el uso de `orElseGet()`.
El método `orElseGet()` solo se ejecutará cuando el valor del optional sea null, en cambio el `orElse()` se ejecuta tanto si pasamos un valor null como no. 
Más información disponible en el siguiente enlace: https://www.baeldung.com/java-optional.
```java
final String name = Optional.ofNullable(null).orElse("Default name");
final String name = Optional.ofNullable(null).orElse(getDefaultName()); // siempre se ejecutará getDefaultName(), tanto si pasamos null como si no.
final String name = Optional.ofNullable(null).orElseGet(this::getDefaultName); // solo se ejecutará getDefaultName() si el valor que se pasa en el optional es null.
```

Lanzar excepciones:
Podemos hacer uso de las excepciones dependiendo de los valores de un optional.
```java
final Optional<String> code = Optional.ofNullable(product.getCode()).orElseThrow(IllegalArgumentException::new)
```

Uso de maps y filters en optionals:
```java
final String nameToUpperCase = Optional.of("Test name")
                .filter(String::isEmpty)
                .map(String::toUpperCase)
                .orElse("DEFAULT NAME");
```

### Streams

Permiten el uso del paradigma de programación funcional en Java a partir de Java 8.

Hacen que la implementación sea más robusta y legible, evitan errores y permiten concatenar operaciones en un stream pipeline.

Un Stream pipeline está compuesto de: fuente -> operación intermedia 1 -> ... -> operación intermedia n -> operación terminal,
* Fuente: puede ser un objeto de tipo Collection, List, Set, etc.
* Operadores intermedios: son operaciones que toman como entrada un stream y devuelven otro stream, algunos ejemplos son: filter, map, sort.
* Operadores terminales: son operaciones que toman como entrada un stream y no devuelven otro stream, algunos ejemplos son: collect, forEach, reduce.

A continuación se muestran algunos ejemplos.

```java
final List<ProductModel> products = getProductService().getAllProductsForCatalogVersion(catalogVersionModel);

// Filter by name not null
final List<ProductModel> productWithName = products.stream()
		.filter(product -> Optional.ofNullable(product.getName()).isPresent())
		.collect(Collectors.toList());

// Sort by price
final List<ProductModel> productsSortedByPriceAsc = products.stream()
		.sorted(Comparator.comparing(ProductModel::getPriceQuantity))
		.collect(Collectors.toList());

// All match
final boolean allBiggerThan0 = products.stream()
		.allMatch(product -> product.getPriceQuantity() > 0);

// Any match
final boolean anyBiggerThan50 = products.stream()
		.anyMatch(product -> product.getPriceQuantity() > 50);

// None match
final boolean noneBiggerThan100 = products.stream()
		.noneMatch(product -> product.getPriceQuantity() > 100);

// Max price
final Optional<ProductModel> optionalMaxPriceProduct = products.stream()
		.max(Comparator.comparing(ProductModel::getPriceQuantity));

// Min price
final Optional<ProductModel> optionalMinPriceProduct = products.stream()
		.max(Comparator.comparing(ProductModel::getPriceQuantity).reversed());

// Group by approval status
final Map<ArticleApprovalStatus, List<ProductModel>> productByApprovalStatus = products.stream()
		.collect(Collectors.groupingBy(ProductModel::getApprovalStatus));

// Most expensive product approved
final Optional<ProductModel> mostExpensiveProductOptional = products.stream()
		.filter(productModel -> productModel.getApprovalStatus().equals(ArticleApprovalStatus.APPROVED))
		.max(Comparator.comparing(ProductModel::getPriceQuantity));

// Count products with name not null and active
final long productsWithNameNameAndActive = products.stream()
	.filter(productModel -> productModel.getApprovalStatus().equals(ArticleApprovalStatus.APPROVED))
	.filter(product -> Optional.ofNullable(product.getName()).isPresent())
	.count();
```

## Core

### items.xml

En este fichero es donde se define el modelo de datos del proyecto. 
Cada tipo de dato se refleja en una tabla en base de datos y sus datos guardados en esta.
A estos se les denominan modelos y se puede acceder a ellos mediante una consulta a la base de datos o a través de otro modelo.

Para crear nuevos tipos y que estos cambios se vean reflejados en el sistema se debe realizar una operación de `update` desde el `HAC` o un `initialize` (desde `HAC` o desde consola).

A continuación se detalla una lista de los distintos tipos que se pueden declarar:

* Colecciones (collectiontypes)
* Enums (enumtypes)
* Mapas (maptypes)
* Relaciones (relations)
* Modelos (itemtypes)

Se debe respetar siempre el orden de los tipos ya que se validará durante la compilación.

Para más información: 
* https://help.sap.com/viewer/d0224eca81e249cb821f2cdf45a82ace/1905/en-US/8bffa9cc86691014bb70ac2d012708bc.html
* https://help.sap.com/viewer/d0224eca81e249cb821f2cdf45a82ace/1905/en-US/8bff7a568669101488a5e40cb7bbd0b9.html

#### Colecciones (collectiontype)

En este apartado se pueden definir collecciones de tres tipos:

* list
* set
* collection

Una vez definido, se podrá utilizar como si fuera un tipo de dato del sistema.

Las colecciones se almacenan en la base de datos en forma de String, que contiene todas las PKs de los objetos que las conforman separadas por comas. El principal inconveniente de las collection es que la cantidad de relaciones posibles está determinado por cuántos caracteres utiliza la base de datos del proyecto para representar un String.

**Su uso está desaconsejado excepto para determinadas ocasiones, por ejemplo, en listas de datos pequeñas en las cuales no se requiera de una relación directa.**

Ejemplo:

```xml
<collectiontypes>
    <collectiontype code="ExampleItemTypeList" elementtype="ExampleItemType" type="list"/>
</collectiontypes>
```

#### Enums (enumtype)

En esta sección se definen los Enums, los cuales son listas de valores constantes.
Es posible añadir el modificador `dynamic=true` mediante el cual se podrán modificar o añadir valores en tiempo de ejecución.

Una vez definido, se podrá utilizar como si fuera un tipo de dato del sistema.

Ejemplo de un enum dinámico:
```xml
<enumtypes>
    <enumtype code="ExampleEnum" dynamic="true">
        <value code="A"/>
        <value code="B"/>
        <value code="C"/>
    </enumtype>
</enumtypes>
```

#### Mapas (maptype)

En esta sección se definen mapas.
Una vez definido, se podrá utilizar como si fuera un tipo de dato del sistema.

```xml
<maptypes>
    <maptype code="ExampleMap"
             argumenttype="java.lang.String"
             returntype="ExampleItemType"
             autocreate="true"
             generate="false"/>
 </maptypes>
```

#### Relaciones (relation)

Representan uniones entre modelos, y permiten definir relaciones 1-N y N-N. 
En la relación, igual que en las colecciones, se puede definir el tipo.

En su definición, se indica el atributo **source**, el cual define qué modelo será la fuente, y el objeto **target**, el cual define el modelo que será el objetivo.
En cada uno de ellos se indica su relación con el otro. Esta puede ser `one` o `many`.
Además, también se debe indicar el nombre del atributo que tendrán en el otro modelo de la relación.

Ejemplo de una relación 1-N:
```xml
<relations>
    <relation code="Product2ExampleItemType" localized="false">
        <sourceElement type="Product" cardinality="one" qualifier="product"/>
        <targetElement type="ExampleItemType" cardinality="many" qualifier="exampleAttribute"/>
    </relation>
</relations>
```

Ejemplo de una relación N-N:
```xml
<relations>
    <relation code="Product2ExampleItemType" localized="false">
        <deployment table="Prod2ExampleItemType" typecode="20000"/>
        <sourceElement type="Product" cardinality="many" qualifier="product"/>
        <targetElement type="ExampleItemType" cardinality="many" qualifier="exampleAttribute"/>
    </relation>
</relations>
```

#### Cronjobs

Son una funcionalidad utilizada para realizar tareas periódicas, programables en el tiempo.

Para más información:

* https://help.sap.com/viewer/d0224eca81e249cb821f2cdf45a82ace/1905/en-US/8b9ce4868669101499b2f0f25ef9395f.html

Un cronJob está compuesto de un trigger (opcional) y un job (obligatorio). 

+ **CronJob**: posee información necesaria en tiempo de ejecución como puede ser el idioma, el usuario, la moneda, etc.
+ **Trigger**: es el tiempo de activación y hace referencia a cuando se ejecutará el CronJob.
+ **Job**: corresponde a la lógica que se ejecutará cuando el CronJob sea activado bien por un trigger o manualmente.

Creación del item CronJob en caso de que se necesite extender `CronJob` para añadir atributos extra:

##### items.xml

```xml
<itemtype code="ExampleCronJob"
          jaloclass="com.hybris.training.core.jalo.ExampleCronJob"
          extends="CronJob">
    <attributes>
        <attribute qualifier="someAttribute" type="java.lang.String">
            <persistence type="property"/>
        </attribute>
    </attributes>
</itemtype>
```

##### spring.xml

Creación del job para el CronJob `ExampleCronJob`:

```xml
<bean id="exampleJob"
      class="com.hybris.training.job.ExampleJob"
      parent="abstractJobPerformable">
    <constructor-arg name="productService" ref="productService"/>
</bean>
```

Clase java con la lógica del job, nótese que extiende `AbstractJobPerformable<ExampleCronJobModel>` del tipo del cronJobModel definido anteriormente y sobrescribiendo el método `perform`:

```java
package com.hybris.training.job;

public class ExampleJob extends AbstractJobPerformable<ExampleCronJobModel> {

    private static final Logger LOG = Logger.getLogger(ExampleJob.class);
    
    private final ProductService productService;

    public ArcProductJob(final ProductService productService) {

        this.productService = productService;
    }

    @Override
    public PerformResult perform(ArcCronJobModel arcCronJobModel) {

        LOG.info("Performing ExampleJob");
        
        // Do something with productService and arcCronJobModel.getSomeAttribute()
        
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }
    
    private ProductService getProductSerive() {

        return productService;
    }
}
```

##### impex

Una vez se ha definido el CronJob y el Job:

* Se crea la relación entre estos.
* Se crea y se le asocia un trigger al CronJob.

```impex
# Registramos el job
INSERT_UPDATE ServicelayerJob; code[unique = true]; springId[unique = true]
                             ; exampleJob; exampleJob

# Creamos un cronJob
INSERT_UPDATE ExampleCronJob; code[unique = true]; job(code); someAttribute; sessionLanguage(isoCode)[default = en]
                     ; exampleCronJob1; exampleJob; example text;

# Creamos el trigger
INSERT_UPDATE Trigger; cronJob(code)[unique = true]; second[default = 0]; minute[default = 5]; hour[default = 3]; day[default = -1]; month[default = -1]; year[default = -1]; relative[default = false]; active[default = false]; maxAcceptableDelay[default = -1]
# Executed every day at 4 AM
                     ; exampleCronJob1; 0; 0; 4; ; ; ; ; true;

```

Una vez finalizados estos pasos, el cronJob `exampleCronJob1` se ejecutará todos los días a las 4 AM.

#### Interceptors

Ejemplo en el cual se crea el bean de un `Prepare interceptor` relacionado con `Product`:

```xml
<bean id="examplePrepareInterceptor"
          class="de.hybris.training.core.interceptor.impl.ExamplePrepareInterceptor"/>

<bean id="examplePrepareMapping"
      class="de.hybris.platform.servicelayer.interceptor.impl.InterceptorMapping">
    <property name="interceptor" ref="examplePrepareInterceptor"/>
    <property name="typeCode" value="Product"/>
</bean>
```

Ejemplo en el cual se muestra el código base de un `Prepare interceptor` relacionado con `Product`:
```java
public class ExamplePrepareInterceptor implements PrepareInterceptor<ProductModel> {
    @Override
    public void onPrepare(final ProductModel productModel, final InterceptorContext interceptorContext) throws InterceptorException {
        // CODE HERE
    }
}
```

#### Modelos (itemtype)

Los items se podrían definir como la definición de una tabla en la base de datos.

Los modelos son la representación de estos items de la base de datos en objetos manejables en java.

Ejemplo en el cual se añade un atributo nuevo a un modelo ya existente en el sistema:
```xml
<itemtype code="ExampleItemType" autocreate="false" generate="false">
    <attributes>
        <attribute qualifier="examplePropertyName" type="java.lang.String">
            <persistence type="property"/>
        </attribute>
    </attributes>
</itemtype>
```

Ejemplo en el cual se crea un modelo nuevo con un atributo clave (code) y uno localizado (name):
```xml
<itemtype code="ExampleItemType">
    <deployment table="ExampleItemType" typecode="22000"/>
    <attributes>
        <attribute qualifier="code" type="java.lang.String">
            <persistence type="property"/>
            <modifiers optional="false" initial="true" unique="true"/>
        </attribute>
        <attribute qualifier="name" type="localized:java.lang.String">
            <persistence type="property"/>
        </attribute>
    </attributes>
</itemtype>
```

Cuando se obtiene el modelo de la base de datos, el tipo de dato que se obtiene sigue el patrón **\*Model**.
Según el ejemplo anterior: ExampleItemTypeModel.

##### Atributos localizados

Los atributos localizados permiten almacenar un valor para cada idioma.
De forma interna, los atributos localizados se almacenan a través de un **mapa de <Language, String>**.

Definición del tipo de dato localizado:
```xml
<maptype code="localized:java.lang.String"
     argumenttype="Language"
     returntype="java.lang.String"
     autocreate="true"
     generate="false"/>
```

> Estos tipos de datos localizados ya están definidos en el sistema.

Ejemplo de un atributo localizado:
```xml
<itemtype code="ExampleItemType" autocreate="false" generate="false">
    <attributes>
        <attribute qualifier="name" type="localized:java.lang.String">
            <persistence type="property"/>
        </attribute>
    </attributes>
</itemtype>
```

##### Atributos dinámicos

Estos son tributos cuyo valor se calcula en el momento en el que se accede al atributo a través del getter, y permiten personalizar el setter.

Ejemplo de atributo dinámico:
```xml
<itemtype code="ExampleItemType" autocreate="false" generate="false">
    <attributes>
        <attribute qualifier="exampleDynamicAttribute" type="java.lang.String">
            <persistence type="dynamic" attributeHandler="exampleDynamicAttributeHandler"/>
            <modifiers write="false"/>
        </attribute>
    </attributes>
</itemtype>
```

Definición del handler para el atributo dinámico `exampleDynamicAttribute` definido anteriormente:

```xml
<bean name="exampleDynamicAttributeHandler" class="de.hybris.training.core.model.attribute.ExampleDynamicAttributeHandler">
</bean>
```

Definición de la clase Java del Handler:

```java
public class ExampleDynamicAttributeHandler extends AbstractDynamicAttributeHandler<String, ExampleItemTypeModel> {
    @Override
    public String get(final ExampleItemTypeModel model) {
        // GETTER CODE HERE
        // Example:
        return model.getName();
    }

    public void set(ExampleItemTypeModel model, String value) {
        // SETTER CODE HERE
        // Example:
        model.setName(value);
    }
}
```
---
## Facades

### Populator

#### spring.xml

Ejemplo de `Populator`:
```xml
<alias name="defaultExampleItemTypeModelPopulator" alias="exampleItemTypeModelPopulator"/>
<bean name="defaultExampleItemTypeModelPopulator" class="com.hybris.training.facades.populators.ExampleItemTypeModelPopulator"/>
```

Ejemplo del código base de un `populator` para el modelo `ExampleItemType` y el data `ExampleData`:
```java
public class ExampleItemTypeModelPopulator implements Populator<ExampleItemTypeModel, ExampleData> {
	@Override
	public void populate(final ExampleItemTypeModel source, final ExampleData target) {
        // CODE HERE
        // Example code:
		target.setCode(source.getCode());
		target.setName(source.getName());
	}
}
```

Ejemplo de `Converter` a `ExampleData` que incluye un `populator` para el modelo `ExampleItemTypeModel`:
```xml
<alias name="defaultExampleConverter" alias="exampleConverter"/>
<bean id="defaultExampleConverter" parent="abstractPopulatingConverter">
    <property name="targetClass" value="de.hybris.training.facades.data.ExampleData"/>
    <property name="populators">
        <list merge="true">
            <ref bean="exampleItemTypeModelPopulator"/>
        </list>
    </property>
</bean>
```

#### beans.xml

Ejemplo de `Data`:
```xml
<bean class="de.hybris.training.facades.data.ExampleData">
    <property name="code" type="String"/>
    <property name="name" type="String"/>
</bean>
```
---
## Backoffice

### backoffice-config.xml

En backoffice es posible customizar algunos puntos de forma sencilla:
* **Explorer-tree**: Es el menú lateral izquierdo principal.
* **Listview**: Es la vista en la cual se ve la lista de datos para un modelo.
* **Editor-area**: Es la vista de detalle de un modelo, esta permite ver y editar los valores.
* **Simple-search**: Es la barra de búsqueda superior dentro del listview. Sirve para realizar búsquedas simples.
* **Advanced-search**: Es la vista que aparece al pulsar sobre el botón a la izquierda de la búsqueda simple. En esta se pueden realizar búsquedas por atributo.
* **Base**: Es el nombre que aparecerá cuando se muestre un elemento del tipo indicado en otro. También se muestra como título al abrir el editor-area.
* **Create-wizard**: Es una vista desde la cual se puede crear un elemento nuevo del tipo indicado.

A continuación se detallan algunos ejemplos:

Ejemplo de `explorer-tree`:
```xml
<context merge-by="module" component="explorer-tree">
    <explorer-tree:explorer-tree>
        <explorer-tree:navigation-node id="hmc_treenode_example_title">
            <explorer-tree:type-node code="ExampleItemType" id="ExampleItemType"/>
        </explorer-tree:navigation-node>
    </explorer-tree:explorer-tree>
</context>
```

Ejemplo de `listview`:
```xml
<context type="ExampleItemType" component="listview">
    <list:list-view>
        <list:column qualifier="code"/>
        <list:column qualifier="name"/>
    </list:list-view>
</context>
```

Ejemplo de `editor-area`:
```xml
<context type="ExampleItemType" merge-by="type" component="editor-area">
    <editorArea:editorArea xmlns:editorArea="http://www.hybris.com/cockpitng/component/editorArea">
        <editorArea:essentials>
            <editorArea:essentialSection name="hmc.essential">
                <editorArea:attribute qualifier="code" readonly="true"/>
            </editorArea:essentialSection>
        </editorArea:essentials>

        <editorArea:tab name="hmc.tab.ExampleItemType.exampleTab" position="1">
            <editorArea:section name="hmc.section.ExampleItemType.exampleSection">
                <editorArea:attribute qualifier="name"/>
            </editorArea:section>
        </editorArea:tab>
    </editorArea:editorArea>
</context>
```

Ejemplo de `simple-search`:
```xml
<context merge-by="type" type="ExampleItemType" component="simple-search">
    <yss:simple-search>
        <yss:field name="code"/>
        <yss:field name="name"/>
    </yss:simple-search>
</context>
```

Ejemplo de `advanced-search`:
```xml
<context type="ExampleItemType" component="advanced-search">
    <advanced-search:advanced-search connection-operator="AND">
        <advanced-search:field-list>
            <advanced-search:field name="code" selected="true"/>
            <advanced-search:field name="name"/>
        </advanced-search:field-list>
    </advanced-search:advanced-search>
</context>
```

Ejemplo de `base`:
```xml
<context type="ExampleItemType" component="base" merge-by="module">
        <y:base>
            <y:labels>
                <y:label>'[' + code + '] ' + (name?:'')</y:label>
            </y:labels>
        </y:base>
    </context>
```

Ejemplo de `create-wizard`:
```xml
<context component="create-wizard" type="ExampleItemType">
    <wz:flow id="ExampleItemTypeWizard" title="create.title(ctx.TYPE_CODE)">
        <wz:prepare id="ExampleItemTypePrepare">
            <wz:initialize property="newExampleItemType" type="ctx.TYPE_CODE"/>
        </wz:prepare>
        <wz:step id="step1" label="ExampleItemTypeWizard.create-wizard.step1.general">
            <wz:content id="step1.content">
                <wz:property-list root="newExampleItemType">
                    <wz:property qualifier="code"/>
                    <wz:property qualifier="name"/>
                </wz:property-list>
            </wz:content>
            <wz:navigation id="step1.navigation">
                <wz:cancel/>
                <wz:done visible="newExampleItemType.code != null">
                    <wz:save property="newExampleItemType"/>
                </wz:done>
            </wz:navigation>
        </wz:step>
    </wz:flow>
</context>
```
